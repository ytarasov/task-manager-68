package ru.t1.ytarasov.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! Permission denied");
    }

}
