package ru.t1.ytarasov.tm.dto.response.user;

import lombok.Getter;
import lombok.Setter;
import ru.t1.ytarasov.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
public class UserLogoutResponse extends AbstractResultResponse {
}
