package ru.t1.ytarasov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.model.IListener;
import ru.t1.ytarasov.tm.event.ConsoleEvent;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show list of terminal commands";

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[HELP]");
        for (final IListener listener : listeners) System.out.println(listener);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
