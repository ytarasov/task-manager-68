package ru.t1.ytarasov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setTaskId(id);
        getTaskEndpoint().removeTaskById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
