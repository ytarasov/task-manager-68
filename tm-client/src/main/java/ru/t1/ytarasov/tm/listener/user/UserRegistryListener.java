package ru.t1.ytarasov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.user.UserRegistryRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user";

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE");
        System.out.println(Arrays.toString(Role.values()));
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @Nullable final Role role = Role.toRole(roleValue);
        @Nullable final String token = getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(token);
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        request.setRole(role);
        getUserEndpoint().registryUser(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
