package ru.t1.ytarasov.tm.api.repository.dto;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;

@Repository
public interface IDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {
}
