package ru.t1.ytarasov.tm.api.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

@Repository
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {
}
