package ru.t1.ytarasov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @NotNull
    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull OperationType type, @NotNull Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
