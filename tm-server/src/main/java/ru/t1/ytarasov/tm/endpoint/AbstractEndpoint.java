package ru.t1.ytarasov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.ytarasov.tm.api.service.IAuthService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

@Getter
@Controller
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    @NotNull
    protected SessionDTO check(@Nullable AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final SessionDTO session = getAuthService().validateToken(token);
        if (session == null) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(
            @Nullable AbstractUserRequest request,
            @Nullable Role role
    ) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final SessionDTO session = getAuthService().validateToken(token);
        if (session == null) throw new AccessDeniedException();
        if (!session.getRole().getDisplayName().equals(role.getDisplayName())) throw new AccessDeniedException();
        return session;
    }

}
