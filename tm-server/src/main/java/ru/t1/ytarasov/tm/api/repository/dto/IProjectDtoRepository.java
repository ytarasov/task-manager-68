package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    List<ProjectDTO> findByUserId(@NotNull final String userId);

    @Nullable
    ProjectDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String id);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);


}
