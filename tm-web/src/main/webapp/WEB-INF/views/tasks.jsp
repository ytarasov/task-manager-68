<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASKS</h1>
<table>
	<th>Project</th>
	<th>ID</th>
	<th>Name</th>
	<th>Description</th>
	<th>Created</th>
	<th>Updated</th>
	<th>Date begin</th>
	<th>Date end</th>
	<th>Status</th>
	<th>Edit</th>
	<th>Delete</th>
	<c:forEach var="task" items="${tasks}">
	<tr>
		<td><c:out value="${projectService.findById(task.projectId).name}" /></td>
		<td><c:out value="${task.id}" /></td>
		<td><c:out value="${task.name}" /></td>
		<td><c:out value="${task.description}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.created}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.updated}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}" /></td>
		<td><c:out value="${task.status.displayName}" /></td>
		<td><a href="/task/edit/${task.id}">EDIT</a></td>
		<td><a href="/task/delete/${task.id}">DELETE</a></td>
    </tr>
    </c:forEach>
</table>
<form action="/task/create" method="POST" style="margin-top: 20px">
    <button>CREATE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>