package ru.t1.ytarasov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.t1.ytarasov.tm.api.endpoint.ProjectRestEndpoint;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ProjectRestEndpointClient implements ProjectRestEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/endpoint/projects/";

    public static void main(String[] args) {
        @NotNull final ProjectRestEndpointClient client = new ProjectRestEndpointClient();
        System.out.printf("Project count: %s%n", client.count());
        System.out.println("\nProject list: ");
        List<ProjectDto> projects = new ArrayList<>(client.findAll());
        projects.forEach(project -> System.out.printf("Project Id: %s, Project Name: %s%n", project.getId(), project.getName()));
        System.out.println("\nCreate new project");
        client.create();
        System.out.println("Project list: ");
        projects = new ArrayList<>(client.findAll());
        projects.forEach(project -> System.out.printf("Project Id: %s, Project Name: %s%n", project.getId(), project.getName()));
        @NotNull final String projectId = projects.get(0).getId();
        System.out.printf("%nProject existing by id: %s is %s%n", projectId, client.existsById(projectId));
        ProjectDto project = client.findById(projectId);
        System.out.printf("%nFind project by id: %s%n", project == null ? "Project not found" : project.getName());
        System.out.printf("Delete project by id: %s%n", projectId);
        client.deleteById(projectId);
        System.out.printf("Project existing by id: %s is %s%n", projectId, client.existsById(projectId));
        System.out.printf("%nRecreate project by id: %s%n", projectId);
        client.save(project);
        System.out.printf("Project existing by id: %s is %s%n", projectId, client.existsById(projectId));
        System.out.printf("%nProject count: %s%n", client.count());
        System.out.println("Delete all project");
        client.deleteAll(projects);
        System.out.printf("Project count: %s%n", client.count());
        System.out.println("\nCreate 10 project");
        for (int i = 0; i < 10; i++) {
            client.create();
        }
        System.out.printf("Project count: %s%n", client.count());
        System.out.println("\nClear repository");
        client.clear();
        System.out.printf("Project count: %s%n", client.count());
        System.out.println("\nCreate 10 project");
        for (int i = 0; i < 10; i++) {
            client.create();
        }
        System.out.printf("Project count: %s%n", client.count());
    }

    @Override
    public void create() {
        @NotNull final String localUrl = "create";
        @NotNull final RestTemplate template = new RestTemplate();
        template.put(ROOT_URL + localUrl, ProjectDto.class);
    }

    @Override
    public void save(ProjectDto project) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(ROOT_URL + localUrl, entity, ProjectDto.class);
    }

    @Override
    public Collection<ProjectDto> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, ProjectDto[].class));
    }

    @Override
    public ProjectDto findById(String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, ProjectDto.class, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    @Override
    public boolean existsById(String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public void delete(ProjectDto project) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(ROOT_URL + localUrl, entity, ProjectDto.class);
    }

    @Override
    public void deleteById(String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public void deleteAll(List<ProjectDto> projects) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(projects, headers);
        template.postForObject(ROOT_URL + localUrl, entity, ProjectDto[].class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

}
