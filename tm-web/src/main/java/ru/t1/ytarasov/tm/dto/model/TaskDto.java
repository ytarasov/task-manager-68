package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDto", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "updated",
        "dateStart",
        "dateFinish",
        "projectId"
})
public class TaskDto {

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @XmlElement(required = true)
    @Column(unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    @XmlElement(required = true)
    private String description;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @NotNull
    @Column(nullable = false)
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @Column(name = "date_start")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Column(name = "date_finish")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateFinish;

    @Nullable
    @XmlElement(required = true)
    private String projectId;

    public TaskDto(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
