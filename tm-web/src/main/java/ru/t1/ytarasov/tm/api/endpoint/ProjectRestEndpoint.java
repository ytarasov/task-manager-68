package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

@RequestMapping("/api/project")
public interface ProjectRestEndpoint {

    @PutMapping("/create")
    void create();

    @PostMapping("/save")
    void save(
            @RequestBody ProjectDto project
    );

    @GetMapping("/findAll")
    Collection<ProjectDto> findAll();

    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @RequestBody ProjectDto project);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id") String id
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<ProjectDto> projects
    );

    @DeleteMapping("/clear")
    void clear();

}
