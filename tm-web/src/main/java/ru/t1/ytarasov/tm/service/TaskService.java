package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.ITaskDtoRepository;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    public void save(@NotNull final TaskDto task) {
        taskDtoRepository.saveAndFlush(task);
    }

    public void create() {
        @NotNull final TaskDto task = new TaskDto("New task " + System.currentTimeMillis(), "New task");
        taskDtoRepository.saveAndFlush(task);
    }

    public Collection<TaskDto> findAll() {
        return taskDtoRepository.findAll();
    }

    public TaskDto findById(@NotNull final String id) {
        return taskDtoRepository.findById(id).orElse(null);
    }

    public long count() {
        return taskDtoRepository.count();
    }

    public boolean existsById(@NotNull final String id) {
        return taskDtoRepository.existsById(id);
    }

    public void delete(@NotNull final TaskDto task) {
        taskDtoRepository.delete(task);
    }

    public void deleteById(@NotNull final String id) {
        taskDtoRepository.deleteById(id);
    }

    public void deleteAll(@NotNull final List<TaskDto> tasks){
        taskDtoRepository.deleteAll(tasks);
    }

    public void clear() {
        taskDtoRepository.deleteAll();
    }

}
