package ru.t1.ytarasov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.dto.soap.*;
import ru.t1.ytarasov.tm.service.ProjectService;

import java.util.Collection;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://ytarasov.t1.ru/tm/dto/soap";

    @Autowired
    private ProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        final ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(projectService.count());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@RequestPayload final ProjectCreateRequest request) {
        projectService.create();
        return new ProjectCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@RequestPayload final ProjectDeleteAllRequest request) {
        projectService.deleteAll(request.getProject());
        return new ProjectDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.delete(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) {
        final boolean exists = projectService.existsById(request.getId());
        final ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        response.setExists(exists);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final Collection<ProjectDto> projects = projectService.findAll();
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProject(projects);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findAll(@RequestPayload final ProjectFindByIdRequest request) {
        final ProjectDto project = projectService.findById(request.getId());
        final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }

}
