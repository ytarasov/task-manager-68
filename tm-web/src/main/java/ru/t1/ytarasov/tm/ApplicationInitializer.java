package ru.t1.ytarasov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.ytarasov.tm.configuration.ApplicationConfiguration;
import ru.t1.ytarasov.tm.configuration.DataBaseConfiguration;
import ru.t1.ytarasov.tm.configuration.WebApplicationConfiguration;
import ru.t1.ytarasov.tm.configuration.WebConfiguration;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class,
                DataBaseConfiguration.class
        };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebApplicationConfiguration.class,
                WebConfiguration.class
        };
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
