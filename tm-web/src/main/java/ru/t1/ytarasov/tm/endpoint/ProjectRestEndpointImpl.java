package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.ProjectRestEndpoint;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.service.ProjectService;

import javax.jws.WebMethod;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @PutMapping("/create")
    public void create() {
        projectService.create();
    }

    @Override
    @PostMapping("/save")
    public void save(
            @NotNull
            @RequestBody final
            ProjectDto project
    ) {
        projectService.save(project);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @PathVariable("id") final
            String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @PathVariable("id") final
            String id
    ) {
        return projectService.existsById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @RequestBody final
            ProjectDto project) {
        projectService.delete(project);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @PathVariable("id") final
            String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @RequestBody final
            List<ProjectDto> projects) {
        projectService.deleteAll(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
