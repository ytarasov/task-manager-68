package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface TaskRestEndpoint {

    @PutMapping("/create")
    void create();

    @PostMapping("/save")
    void save(
            @RequestBody TaskDto task
    );

    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @GetMapping("/findById/{id}")
    TaskDto findById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @WebParam(name = "id")
            @RequestBody TaskDto task
    );

    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<TaskDto> tasks
    );

    @DeleteMapping("/clear")
    void clear();

}
