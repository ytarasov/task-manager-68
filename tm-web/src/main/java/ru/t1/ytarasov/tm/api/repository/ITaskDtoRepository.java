package ru.t1.ytarasov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {
}
