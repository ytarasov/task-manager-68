package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.IProjectDtoRepository;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    public void save(@NotNull final ProjectDto project) {
        projectDtoRepository.saveAndFlush(project);
    }

    public void create() {
        @NotNull final ProjectDto project = new ProjectDto("New project " + System.currentTimeMillis(),
                "New project");
        projectDtoRepository.saveAndFlush(project);
    }

    public Collection<ProjectDto> findAll() {
        return projectDtoRepository.findAll();
    }

    public ProjectDto findById(@NotNull final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    public long count() {
        return projectDtoRepository.count();
    }

    public boolean existsById(@NotNull final String id) {
        return projectDtoRepository.existsById(id);
    }

    public void delete(@NotNull final ProjectDto project) {
        projectDtoRepository.delete(project);
    }

    public void deleteById(@NotNull final String id) {
        projectDtoRepository.deleteById(id);
    }

    public void deleteAll(@NotNull final List<ProjectDto> projects) {
        projectDtoRepository.deleteAll(projects);
    }

    public void clear() {
        projectDtoRepository.deleteAll();
    }
    
}
