package ru.t1.ytarasov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.dto.soap.*;
import ru.t1.ytarasov.tm.service.TaskService;

import java.util.Collection;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://ytarasov.t1.ru/tm/dto/soap";

    @Autowired
    private TaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        final long count = taskService.count();
        final TaskCountResponse response = new TaskCountResponse();
        response.setCount(count);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse create(@RequestPayload final TaskCreateRequest request) {
        return new TaskCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(@RequestPayload final TaskDeleteAllRequest request) {
        taskService.deleteAll(request.getTask());
        return new TaskDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.delete(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@RequestPayload final TaskExistsByIdRequest request) {
        final boolean exists = taskService.existsById(request.getId());
        final TaskExistsByIdResponse response = new TaskExistsByIdResponse();
        response.setExists(exists);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final Collection<TaskDto> tasks = taskService.findAll();
        final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTask(tasks);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        final TaskDto task = taskService.findById(request.getId());
        final TaskFindByIdResponse response = new TaskFindByIdResponse();
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

}
