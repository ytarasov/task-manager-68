package ru.t1.ytarasov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.t1.ytarasov.tm.api.endpoint.TaskRestEndpoint;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/endpoint/tasks/";

    public static void main(String[] args) {
        @NotNull final TaskRestEndpointClient client = new TaskRestEndpointClient();
        System.out.printf("Task count: %s%n", client.count());
        System.out.println("\nTask list: ");
        List<TaskDto> tasks = new ArrayList<>(client.findAll());
        tasks.forEach(task -> System.out.printf("Task Id: %s, Task Name: %s%n", task.getId(), task.getName()));
        System.out.println("\nCreate new task");
        client.create();
        System.out.println("Task list: ");
        tasks = new ArrayList<>(client.findAll());
        ;
        tasks.forEach(task -> System.out.printf("Task Id: %s, Task Name: %s%n", task.getId(), task.getName()));
        @NotNull final String taskId = tasks.get(0).getId();
        System.out.printf("%nTask existing by id: %s is %s%n", taskId, client.existsById(taskId));
        TaskDto task = client.findById(taskId);
        System.out.printf("%nFind task by id задачи: %s%n", task == null ? "Task not found" : task.getName());
        System.out.printf("Delete task by id: %s%n", taskId);
        client.deleteById(taskId);
        System.out.printf("Task existing by id: %s is %s%n", taskId, client.existsById(taskId));
        System.out.printf("%nRecreate task by id: %s%n", taskId);
        client.save(task);
        System.out.printf("Task existing by id: %s is %s%n", taskId, client.existsById(taskId));
        System.out.printf("%nTask count: %s%n", client.count());
        System.out.println("Delete all task");
        client.deleteAll(tasks);
        System.out.printf("Task count: %s%n", client.count());
        System.out.println("\nCreate 10 task");
        for (int i = 0; i < 10; i++) {
            client.create();
        }
        System.out.printf("Task count: %s%n", client.count());
        System.out.println("\nClear repository");
        client.clear();
        System.out.printf("Task count: %s%n", client.count());

        System.out.println("\nCreate 10 task");
        for (int i = 0; i < 10; i++) {
            client.create();
        }
        System.out.printf("Task count: %s%n", client.count());
    }

    @Override
    public void create() {
        @NotNull final String localUrl = "create";
        @NotNull final RestTemplate template = new RestTemplate();
        template.put(ROOT_URL + localUrl, TaskDto.class);
    }

    @Override
    public void save(TaskDto task) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        template.postForObject(ROOT_URL + localUrl, entity, TaskDto.class);
    }

    @Override
    public Collection<TaskDto> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, TaskDto[].class));

    }

    @Override
    public TaskDto findById(String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, TaskDto.class, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    @Override
    public boolean existsById(String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public void delete(TaskDto task) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        template.postForObject(ROOT_URL + localUrl, entity, TaskDto.class);
    }

    @Override
    public void deleteById(String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public void deleteAll(List<TaskDto> tasks) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(tasks, headers);
        template.postForObject(ROOT_URL + localUrl, entity, TaskDto[].class);

    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

}
